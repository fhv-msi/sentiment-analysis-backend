package at.fhv.sentiment_analysis.handlers;

// Start of user code (user defined imports)
import org.apache.commons.codec.digest.DigestUtils;
import java.util.HashMap;
import java.util.Map;
// End of user code

public class AuthHandler {
	// Start of user code (user defined attributes)
	private Map<String, at.fhv.sentiment_analysis.models.User> users = new HashMap<>();
	// End of user code
	

	private static AuthHandler INSTANCE;
	
	private AuthHandler(){
	    // singleton
	}
	
	public static AuthHandler getInstance(){
	    if(INSTANCE == null){
	        INSTANCE = new AuthHandler();
	    }
	
	    return INSTANCE;
	}
	
	public void logout(String token) throws Exception {
		// Start of user code logout
		users.remove(token);
		// End of user code
	}
	
	public String login(String email) throws Exception {
		// Start of user code login
		 String token = DigestUtils.md2Hex(email);
	        if (!users.containsKey(token)) {
	        	at.fhv.sentiment_analysis.models.User user = new at.fhv.sentiment_analysis.models.User();
	        	user.setEmail(email);
	        	user.setHistory(new at.fhv.sentiment_analysis.models.History());
	            users.put(token, user);
	        }

	        return token;
		// End of user code
	}
	
	// Start of user code (user defined operations)
	public at.fhv.sentiment_analysis.models.User getUser(String token){
        return users.get(token);
    }
	// End of user code
	
}
